# TeamBot Script

## About:
This is a script which can be used to manage teambot

### Features:
- One command install of teambot, no need to worry about doing it yourself
- Dynamic updating, automatically checks for updates and updates teambot
- Simple start/stop/restarting command to allow quick and simple management of the bot
- Config walkthrough, no need to learn json, the script will walk you through every requirement needed!

## Requirements:
- Python 3.8 or higher (It may work on python 3.7, however this has not been tested)
- Requests package, can be installed through the running of the script initially

## Current status:
- Dynamic versioning needs to be added to the main repo before it can be introduced into the script
- No systemd config added in the install, as this would be needed to be added in the main repo, therefore an update is needed
- updating, starting and stopping and restarting needs to be added
- no support for custom directory installs