#!/usr/bin/python3

requirements = ["requests"]
import sys
import os
try:
    import requests
except ModuleNotFoundError:
    print("Installing TeamBot Script dependencies...")
    try:
        import pip
        req_string = ""
        for package in requirements:
            req_string += "'" + package + "' "
        while True:
            print("TeamBot Script requires: " + req_string + " in order to be executed, would you like to install them? (y/n)")
            reply = input()
            if reply == "y":
                break
            elif reply == "n":
                print("You must install the requirements in order to run teambot!")
                exit(1)
        for package in requirements:
            pip.main(['install', package])
        print("Successfully installed requirements!")
    except Exception as error:
        print("Failed to install dependencies:\n" + error)
    


class TeamBot:
    
    def __init__(self):
        self.tb_repo_id = "20910821"
        self.script_version = "1.0.0"
        self.tb_version = "1.1.9"  # as of current teambot version, there is no way to find the version externally
        self.args = sys.argv[1:]
        self.commands = {"help": self.help_command,
                          "version": self.version_command,
                          "install": self.install_command,
                          "uninstall": self.uninstall_command,
                          "update": self.update_command,
                          "start": self.start_command,
                          "stop": self.stop_command}
        
        try:
            method = self.commands[self.args[0].lower()]
        except KeyError:
            print("Command not found!")
            return
        except IndexError:
            print("Please specify a command, for a list of commands run:\npython3 teambot.py help")
            return
        method()
    
    @staticmethod
    def help_command():
        print("Commands:\nHelp - Displays this command\nVersion - Displays teambot's current version")
    
    def version_command(self):
        print("TeamBot Script version: " + self.script_version)
        print("TeamBot version: " + self.tb_version)
            
    
    def install_command(self):
        if os.path.exists("tb_data"):
            print("Team Bot already installed, if you would like to reinstall run and then rerun this command:\nrm -r tb_data")
            return
        print("Searching for the latest version...")
        resp = requests.get("https://gitlab.com/api/v4/projects/" + self.tb_repo_id + "/releases")
        if resp.status_code != 200:
            print("An error has occurred when searching for the lastest version!")
            return
        releases = resp.json()
        latest_release = releases[0]
        print("Found latest version: " + latest_release['name'] + " (" + latest_release['tag_name'] + ")")
        print("Fetching code...")
        tarball = latest_release['assets']['sources'][1]['url']
        os.system("wget " + tarball)
        print("Decompressing code...")
        file_name = tarball.split("/")[-1]
        os.system("tar -xvzf " + file_name)
        print("Successfuly Decompressed code!\nMaking teambot directory (tb_data)")
        os.system("mkdir tb_data")
        print("Removing tarball...")
        os.system("rm " + file_name)
        print("moving code into teambot directory...")
        os.system("mv {} ./tb_data/team-discord-bot".format(file_name).replace(".tar.gz", ""))
        print("Successfully installed teambot!")
        print("WARNING: do not rename the tb_data directory, this script must be left outside of the directory!")

    def uninstall_command(self):
        while True:
            print("Are you sure you would like to uninstall teambot completely? (y/n)\nWARNING: ALL DATA WILL BE LOST!")
            reply = input()
            if reply == "y":
                break
            elif reply == "n":
                print("Uninstall aborted!")
                return
        os.system("rm -r tb_data")
        print("Removed all teambot data!")
    
    def update_command(self):
        pass
    
    def start_command(self):
        pass
    
    def stop_command(self):
        pass


if __name__ == "__main__":
    tb = TeamBot()
        